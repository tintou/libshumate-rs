// Take a look at the license at the top of the repository in the LICENSE file.

// rustdoc-stripper-ignore-next
//! Traits intended for creating custom types.

pub mod data_source;
pub mod location;
pub mod map_source;
pub mod marker;

// rustdoc-stripper-ignore-next
/// Traits intended for blanket imports.
pub mod prelude {
    #[doc(hidden)]
    pub use gtk::subclass::prelude::*;

    pub use super::data_source::{DataSourceImpl, DataSourceImplExt};
    pub use super::location::{LocationImpl, LocationImplExt};
    pub use super::map_source::{MapSourceImpl, MapSourceImplExt};
    pub use super::marker::MarkerImpl;
}
