// Take a look at the license at the top of the repository in the LICENSE file.

// rustdoc-stripper-ignore-next
//! Traits intended for subclassing [`Marker`](crate::Marker).

use crate::Marker;
use glib::subclass::prelude::*;
use gtk::subclass::prelude::WidgetImpl;

pub trait MarkerImpl: WidgetImpl {}

unsafe impl<T: MarkerImpl> IsSubclassable<T> for Marker {}
