# libshumate-rs

The Rust bindings of [libshumate](https://gitlab.gnome.org/GNOME/libshumate)

Website: <https://world.pages.gitlab.gnome.org/Rust/libshumate-rs>

## Documentation

- libshumate: <https://world.pages.gitlab.gnome.org/Rust/libshumate-rs/stable/latest/docs/libshumate>
- libshumate-sys: <https://world.pages.gitlab.gnome.org/Rust/libshumate-rs/stable/latest/docs/libshumate_sys>
